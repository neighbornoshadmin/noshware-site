import React, { Component } from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom';

import TopBar from './components/TopBar/topBar';
import LandingPage from './containers/LandingPage/appPage';
import AboutUs from './containers/AboutUs/aboutUs';
import OurTech from './containers/Tech/techPage';
import Footer from './components/Footer/Footer';

import './App.css';
import SideBar from './components/SideBar/sideBar';
import InvoiceForm from './components/InvoiceForm/invoiceForm';
import Logo from './components/Logo/logo';

class App extends Component {
  render() {

    const openSideBar = () => {
      document.getElementById("mySideBar").style.width = "100%";
      document.getElementById("myInvoiceForm").style.width = "0";
    }

    return (
      <Router>
      <div className="mainContainer">
        <div className="App">
          <header className="App-header">
            <i onClick={openSideBar} className="fas fa-bars fa-2x"></i>
            <Logo />
            <TopBar />
            <SideBar />
            <InvoiceForm />
          </header>
          <main>
            <Route exact path="/" component={LandingPage}></Route>
            <Route exact path="/aboutUs" component={AboutUs}></Route>
            <Route exact path="/tech" component={OurTech}></Route>
          </main>
          <footer>
            <Footer />
          </footer>
        </div>
      </div>
      </Router>
    );
  }
}

export default App;
