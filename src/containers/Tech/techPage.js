import React from 'react';

import IconGrid from '../../components/IconGrid/iconGrid';
import TechDescription from '../../components/TechDescription/techDescription';

const OurTech = () => {
    return (
        <div>
            <IconGrid />
            <TechDescription />
        </div>
    )
}

export default OurTech;