import React from 'react';

import Intro from '../../components/AboutIntro/aboutIntro';
import Services from '../../components/Services/services';
import ClienteleSlideshow from '../../components/ClienteleSlideShow/clienteleSlideshow';
import MachineSlideshow from '../../components/MachineSlideshow/machineSlideshow';

const LandingPage = () => {
    return (
        <div>
            <ClienteleSlideshow />
            <Intro />
            <Services />
            <MachineSlideshow />
        </div>
    );
}

export default LandingPage;