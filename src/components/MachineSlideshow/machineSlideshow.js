import React from 'react';
import { Slide } from 'react-slideshow-image';
import { withStyles } from '@material-ui/core/styles';

import './machineSlideshow.css';

import MockUp from '../../assets/ClientImages/Mockup.jpg';
import ComboPlus from '../../assets/DeviceImages/comboplus.png';
import MultiSeller from '../../assets/DeviceImages/Vision_Multiseller.png';
import MachineCloseUp from '../../assets/DeviceImages/MachineCloseUp.JPG';

const clienteleImages = [
    MockUp,
    ComboPlus,
    MultiSeller,
    MachineCloseUp
];

const properties = {
    duration: 5000,
    transitionDuration: 300,
    infinite: true,
    indicators: false,
    arrows: true
}

const styles = theme => ({
    slide1: {
        backgroundImage: `url(${clienteleImages[0]})`,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundSize: '100% 100%',
        backgroundRepeat: 'no-repeat',
        width: '100%',
        height: '300px'
    },
    slide2: {
        backgroundImage: `url(${clienteleImages[1]})`,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundSize: '100% 100%',
        height: '300px'
    },
    slide3: {
        backgroundImage: `url(${clienteleImages[2]})`,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundSize: '100% 100%',
        height: '300px'
    },
    slide4: {
        backgroundImage: `url(${clienteleImages[3]})`,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundSize: '100% 100%',
        height: '300px'
    },
});

const MachineSlideshow = (props) => {
    const { classes } = props;

    return (
        <div className="machine-slide-container">
            <Slide {...properties}>
              <div className={classes.slide1}>
                <div>
                  <span>Slide 1</span>
                </div>
              </div>
              <div className={classes.slide2}>
                {/* <div>
                  <span>Slide 2</span>
                </div> */}
              </div>
              <div className={classes.slide3}>
              </div>
              <div className={classes.slide4}>
              </div>
            </Slide>
        </div>
    )
}

export default withStyles(styles)(MachineSlideshow);