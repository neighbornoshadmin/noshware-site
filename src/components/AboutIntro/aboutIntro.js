import React from 'react';

import './aboutIntro.css';

const Intro = () => {
    return (
        <div>
            <section className="aboutUsIntro">
                <div>
                <h2 className="aboutCaps">ABOUT US</h2>
                <p className="noshWareBioIntro">NTI was started in Southern California as 
                an extention of NeighborNosh Incorporated, our specialty fresh food vending brand.  
                When developing NeighborNosh we noticing a void in available customizable vending software and 
                began our mission to build a product that is not only suited our own needs but was also affordable, 
                unique, customizable, futuristic and 
                fun custom experience for any marketing event or consumer facing vending experience.
                </p>
                </div>
            </section>
        </div>
    );
}

export default Intro;