import React from 'react';

import './slogan.css';

const Slogan = () => {
    return(
        <div>
            <section className="vendIt">
                <p className="sloganCursive">You dream it</p>
                <hr />
                <span className="sloganAnton">WE VEND IT</span>
            </section>
        </div>
    );
}

export default Slogan;