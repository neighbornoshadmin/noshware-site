import React from 'react';

import NTILogo from '../../assets/NTILogo.png';

import './logo.css';

const Logo = () => {
    return (
        <div>
            <h2 className="banner"><img src={NTILogo} className="LogoImage" alt="Noshware Technologies Incoporated logo" />Noshware Technologies INC</h2>
        </div>
    );
}

export default Logo;