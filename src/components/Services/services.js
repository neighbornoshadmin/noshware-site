import React from 'react';
import { withStyles } from '@material-ui/core/styles';

import './services.css';

const styles = theme => ({
    root: {
        flexGrow: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: '3rem',
        paddingRight: '3rem',
    },
});

const Services = (props) => {
    const { classes } = props;

    return (
        <div>
            <section className="servicesBlock">
                <h2 className="servicesAnton">OUR MACHINES</h2>
                <div className={classes.root}>
                </div>
            </section>
        </div>
    );
}

export default withStyles(styles)(Services);