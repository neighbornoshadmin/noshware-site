import React from 'react';
import { withStyles } from '@material-ui/core/styles';

import './Footer.css';

const styles = theme => ({
    root: {
        flexGrow: 1,
        paddingTop: '1rem',
    },
    ContactButton: {
        background: "rgb(246, 157, 1)",
        textTransform: "uppercase",
        padding: "0.5rem 3rem",
        borderRadius: "15px",
        fontFamily: "Anton",
        fontSize: "1.5rem",
        letterSpacing: "1px"
    },
    copyRight: {
        fontWeight: "bold"
    }
})

const Footer = (props) => {
    const { classes } = props;
    return (
        <div>
            <div className={classes.root}>
                <a href="/" className={classes.ContactButton}>Contact</a>
                <p className={classes.copyRight}>{[ String.fromCharCode(169) + '2019 Noshware Technologies Incorporated']}</p>
            </div>
        </div>
    );
}

export default withStyles(styles)(Footer);