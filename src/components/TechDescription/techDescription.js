import React from 'react';

import './techDescription.css';
import machine from '../../assets/VendingMachine.png';

const TechDescription = () => {
    return (
        <div>
            <section className="techDescription">
                <span>At Noshware, our team is dedicated to use the latest technology 
                    to ensure our machines are performaning at an optimal level.
                </span>
                <img className="vendingMachine" src={machine} alt="Vending Machine" />
            </section>
        </div>
    )
}

export default TechDescription;