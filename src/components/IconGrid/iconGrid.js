import React from 'react';

import './iconGrid.css';

const IconGrid = () => {
    return (
        <div>
            <div className="techBox">
                <h4 className="stackTitle">Our Stack:</h4>
                <div className="iconGrid">
                {/* DevIcon made by Konpa: https://github.com/konpa */}
                    <i className="devicon-amazonwebservices-original colored"></i>
                    <i className="devicon-debian-plain colored"></i>
                    <i className="devicon-react-original colored"></i>
                    <i className="devicon-css3-plain colored"></i>
                    <i className="devicon-nodejs-plain colored"></i>
                    <i className="devicon-babel-plain colored"></i>
                    <i className="devicon-webpack-plain colored"></i>
                </div>
            </div>
        </div>
    )
}

export default IconGrid;