import React from 'react';

import './sideBar.css';

const closeSideBar = () => {
    document.getElementById("mySideBar").style.width = "0";
}


const SideBar = () => {

    return (
        <nav id="mySideBar" className="sideBar">
            <div aria-label="Close" className="closeBtn">
                <i className="far fa-window-close fa-2x" onClick={closeSideBar}></i>
            </div>
            <a href="/">HOME</a>
            <a href="/aboutUs">ABOUT US</a>
            <a href="/tech">OUR TECHNOLOGY</a>
        </nav>
    )
}

export default SideBar;