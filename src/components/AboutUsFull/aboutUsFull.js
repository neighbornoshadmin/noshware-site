import React from 'react';

import './aboutUsFull.css';

const AboutUsFull = () => {
    return (
        <div>
            <section className="aboutUsFull">
                <div>
                    <h2>
                        <span className="aboutCaps">ABOUT </span> 
                        <span className="usCaps">US</span>
                    </h2>
                    <div className="row">
                    <span className="col-2">[placeholder for image]</span>
                    <span className="noshWareBio col-2">NTI was started in Southern California as 
                    an extention of NeighborNosh Incorporated, 
                    our specialty fresh food vending brand.  When developing NeighborNosh we noticing 
                    a void in available customizable vending software and began our mission to build a product that is not only suited our own needs but was also affordable, unique, customizable, futuristic and fun custom experience for any marketing event or consumer facing vending experience.
                    </span>
                    </div>
                </div>
            </section>
        </div>
    )
}

export default AboutUsFull;