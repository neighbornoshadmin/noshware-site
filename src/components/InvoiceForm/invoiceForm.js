import React from 'react';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";


import './invoiceForm.css';

const closeInvoiceForm = () => {
    document.getElementById("myInvoiceForm").style.width = "0";
}

const addDays = (startDate,numberOfDays) => {
    var returnDate = new Date(
    startDate.getFullYear(),
    startDate.getMonth(),
    startDate.getDate()+numberOfDays,
    startDate.getHours(),
    startDate.getMinutes(),
    startDate.getSeconds());
    return returnDate;
}

class InvoiceForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            startDate: addDays(new Date(), 14)
        };
        this.handleChange = this.handleChange.bind(this)
    }
    handleChange(date) {
        this.setState({
            startDate: date
        });
    }

    render() {
    return (
        <div id="myInvoiceForm" className="invoiceForm">
            <div aria-label="Close" className="closeForm">
            <i className="far fa-window-close fa-2x" onClick={closeInvoiceForm}></i>
            </div>
            <form id="inquiry" action="mailto:garciajvincent@gmail.com" method="post" encType="text/plain">
                    <legend>SEND US AN INQUIRY!</legend>

                    <label>Full Name: </label>
                    <input id="fullName" type="text" aria-label="Full Name Input" required/>

                    <label>Email: </label>
                    <input id="email" type="email" aria-label="email" required/>

                    <label>
                        When is your event?
                        <span className="disclaimer"><i>Disclaimer: Extra fee for 2 weeks from now</i></span>
                    </label>
                    <label title="Date Picker">
                    <DatePicker
                        id="datePicker" 
                        className="datePicker"
                        selected={this.state.startDate}
                        onChange={this.handleChange}
                        minDate={addDays(new Date(), 14)}
                    />
                    </label>

                    <label>What product do you want to stock in the machine?</label>
                    <textarea rows="5" cols="25" aria-label="Product Input" required/>

                    <label>How long of a rental are you requesting?</label>
                    <input type="text" aria-label="Rental Input" required/>

                    <label>Do you have any size restrictions or a specific footprint we need to keep in mind?</label>
                    <textarea rows="3" cols="25" aria-label="Size Restriction Input" required/>

                    <label>Do you require a technician onsite?</label>
                    <select aria-label="Technician Options">
                        <option value=" "></option>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>
                    </select>

                    <label>Are you looking for any custom built hardware or software applications?</label>
                    <select aria-label="Custom Hardware">
                        <option value=" "></option>
                        <option value="Yes">Yes</option>
                        <option value="No">No</option>
                    </select>

                    <button aria-label="Submit" className="submitForm" type="submit">Submit</button>
            </form>
        </div>
    )
    }
}

export default InvoiceForm;