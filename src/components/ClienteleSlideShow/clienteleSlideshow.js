import React from 'react';
import { Slide } from 'react-slideshow-image';
import { withStyles } from '@material-ui/core/styles';

import './clienteleSlideshow.css';

import SpriteDemo from '../../assets/ClientImages/SpriteDemo.JPG';
import SpriteOverlay from '../../assets/ClientImages/Sprite_overlaySample.png';
import SpriteGlow from '../../assets/ClientImages/SpriteGlow.JPG';
import Mockup from '../../assets/ClientImages/Mockup.jpg';

const clienteleImages = [
    SpriteDemo,
    SpriteOverlay,
    SpriteGlow,
    Mockup
];

const properties = {
    duration: 5000,
    transitionDuration: 300,
    infinite: true,
    indicators: false,
    arrows: true
}

const styles = theme => ({
    slide1: {
        backgroundImage: `url(${clienteleImages[0]})`,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundSize: '100% 100%',
        backgroundRepeat: 'no-repeat',
        width: '100%',
        height: '300px'
    },
    StartButton: {
      background: "rgb(246, 157, 1)",
      textTransform: "uppercase",
      padding: "0.5rem 3rem",
      borderRadius: "15px",
      fontFamily: "Anton",
      fontSize: "1.5rem",
      letterSpacing: "1px"
    },
    slide2: {
        backgroundImage: `url(${clienteleImages[1]})`,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundSize: '100% 100%',
        height: '300px'
    },
    slide3: {
        backgroundImage: `url(${clienteleImages[2]})`,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundSize: '100% 100%',
        height: '300px'
    },
    slide4: {
        backgroundImage: `url(${clienteleImages[3]})`,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundSize: '100% 100%',
        height: '300px'
    },
});

const ClienteleSlideshow = (props) => {
    const { classes } = props;

    return (
      <Slide {...properties}>
        <div className={classes.slide1}>
          <div>
            <a href="/" className={classes.StartButton}>Get Started</a>
          </div>
        </div>
        <div className={classes.slide2}>
          {/* <div>
            <span>Slide 2</span>
          </div> */}
        </div>
        <div className={classes.slide3}>
          {/* <div>
            <span>Slide 3</span>
          </div> */}
        </div>
        <div className={classes.slide4}>
          {/* <div>
            <span>Slide 3</span>
          </div> */}
        </div>
      </Slide>
    )
}

export default withStyles(styles)(ClienteleSlideshow);