import React from 'react';

import './topBar.css';

// const openInvoiceForm = () => {
//     document.getElementById("myInvoiceForm").style.width = "80%";
//     document.getElementById("mySideBar").style.width = "0";
// }

const TopBar = () => {
    return (
        <div>
            {/* Included h1 for Screen Reader Accessibility*/}
            <h1>Noshware Site</h1>
            <nav className="topBar">
                <ul>
                    <li><a href="/" className="topBarOption">Marketing Events</a></li>
                    <li><a href="/" className="topBarOption">Retail Services</a></li>
                    <li><a href="/" className="topBarOption">Vending Machines</a></li>
                    <li><a href="/" className="topBarOption">Software</a></li>
                    <li><a href="/aboutUs" className="topBarOption">About Us</a></li>
                    <li><a href="/tech" className="topBarOption">Custom</a></li>
                    <li><a href="/tech" className="topBarOption">Client Login</a></li>
                </ul>
            </nav>
            {/* <button className="invoiceFormButton" onClick={openInvoiceForm}>Send an Inquiry</button> */}
            
        </div>
    );
}

export default TopBar;